data TT = TTBool
    | Arrow TT TT
    deriving (Eq,Show)

data Term = TTrue
      | TFalse
      | TIf Term Term Term
      | Var String
      | Lambda String TT Term
      | Apply Term Term
      deriving Show

data Binding = NameBind
             | VarBind TT

addBinding :: Context -> String -> Binding -> Context
addBinding context x bind = (x, bind):context

getBinding  :: Context -> String -> Binding
getBinding (x:xs) name | fst x == name = snd x
                       | otherwise = getBinding xs name

type Context = [(String, Binding)]

 

typeOf :: Context -> Term -> TT
typeOf context TTrue = TTBool
typeOf context TFalse = TTBool
typeOf ctx l@(Var name)
    = let (VarBind ttype) = getBinding ctx name
      in ttype

typeOf context (TIf pred a b)
    | typeOf context pred /= TTBool = error "pred must be a bool"
    | otherwise = let typeA = typeOf context a
                      typeB = typeOf context b
                  in if typeA == typeB
                     then typeA
                     else error "then and else branches' types of the if statement are not equal"

typeOf context (Lambda name tVar body)
       = let ffc = addBinding context name $ VarBind tVar
             typeBody = typeOf ffc body
         in Arrow tVar typeBody

typeOf context (Apply t1 t2)
    = let typeT1 = typeOf context t1
          typeT2 = typeOf context t2
      in case typeT1 of
           (Arrow tyArr1 tyArr2) -> if typeT2 == tyArr1
                                     then tyArr2
                                     else error "type mismatch in Apply"
           otherwise -> error "Apply must be of an arrow type"

getType tt = typeOf [] tt



tst1 = Lambda "x" TTBool TTrue

-- (Bool->Bool) -> (Bool->Bool)
tst2 = Lambda "x" (Arrow TTBool TTBool) (Var "x")

-- (\x:Bool->Bool.x) (\y:Bool.y)
tst3 = Apply (Lambda "x" (Arrow TTBool TTBool) (Var "x")) (Lambda "y" TTBool (Var "y"))

