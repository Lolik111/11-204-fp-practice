
-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt a [] = []
deleteInt a [x] = if a == x then []
                         else [x]
deleteInt a (x:y:xs) = if a == x then deleteInt a $ y:xs
                              else x:(deleteInt a $ y:xs)
-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt :: Int -> [Int] -> [Int]
--findIndicesInt a [] = []
findIndicesInt a (x:xs) = let
               fun :: Int -> Int -> [Int] -> [Int] -> [Int]
               fun a b [] (y:ys) = y : ys
               fun a b [] [] = []
               fun a b (x:xs) [] = if a == x then fun a (b + 1) (xs) ([b])
                                             else fun a (b + 1) (xs)  ([])
               fun a b (x:xs) (y:ys) = if a == x then fun a (b + 1) (xs) (y:ys ++ [b])
                                               else fun a (b + 1) (xs)  (y:ys)
               in fun a 0 (x:xs) []

-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo n = undefined

-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int

