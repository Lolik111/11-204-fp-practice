
-- Функция декодирования двоичной записи числа
-- (аналогично декодированию азбуки Морзе, но
-- учтите бесконечность дерева -- нужна ленивость)
decodeBinary :: String -> Integer
decodeBinary str = let decode i [] = i
                       decode i ('0':xs) = decode (i * 2) xs
                       decode i ('1':xs) = decode (i * 2 + 1) xs
                   in decode 0 str


-- Функция декодирования записи числа в системе
-- Фибоначчи: разряды -- числа Фибоначчи, нет
-- двух единиц подряд:
--    0f = 0
--    1f = 1
--   10f = 2
--  100f = 3
--  101f = 4
-- 1000f = 5
-- 1001f = 6
-- 1010f = 7
--   .....
-- (аналогично декодированию азбуки Морзе, но
-- учтите бесконечность дерева -- нужна ленивость)
decodeFibo :: String -> Integer
decodeFibo str = let decode a b [] = 0
                     decode a b ('0':xs) = decode b (a + b) xs
                     decode a b ('1':xs) = a + b + decode b (a + b) xs
                 in decode 0 1 (reverse str)
