pi1 n = let fun a = (2 * a + 1) * (2 * a + 1)
            func :: Double -> Double -> Double
            func 0 b = 2
            func a b = 2 + fun b / (func (a - 1) (b + 1))
            in 4 / (func n 0 - 1)
            
pi2 n = let fun a = (2 * a + 1) * (2 * a + 1)
            func :: Double -> Double -> Double
            func 0 b = 6
            func a b = 6 + fun b / (func (a - 1) (b + 1))
            in func n 0 - 3
            
pi3 n = let fun a = (a + 1) * (a + 1)
            func :: Double -> Double -> Double
            func 0 b = 2
            func a b = 2 * b + 1 + fun b / (func (a - 1) (b + 1))
            in 4 / (func n 0)

pi4 :: Int -> Double
pi4 n = let fun x = 2 * x - 1
            func :: Int -> Double -> Double
            func 0 b = b
            func a b | a `mod` 2 == 1 = func (a - 1)  (b + (1 / fromIntegral(fun a)))
                     | otherwise = func (a - 1) (b - (1 / fromIntegral(fun a)))
            in 4 * (func n 0)
            
pi5 :: Int -> Double
pi5 n = let fun :: Int -> Double
            fun x = 4 / fromIntegral((2 * x) * (2 * x + 1) * (2 * x + 2))
            func :: Int -> Double -> Double
            func 0 b = b
            func a b | a `mod` 2 == 1 = func (a - 1)  (b + (fun a))
                     | otherwise = func (a - 1) (b - (fun a))
            in (func n 0) + 3

e :: Double -> Int -> Double
e x n = let fac :: Int -> Double -> Double
            fac n x = fac' n 1 x
                 where fac' :: Int -> Double -> Double -> Double
                       fac' 1 acc x = acc
                       fac' n acc x | n>1 = fac' (n-1) (acc * x /fromIntegral(n)) x
            func :: Int -> Double -> Double -> Double
            func 0 b x = b
            func a b x = func (a - 1) (b + (fac a x)) x
            in (func n 0 x) + 1