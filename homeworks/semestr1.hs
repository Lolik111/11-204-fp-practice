
--(λx.t) y

--  Его левая часть — (λx.t) — это функция с одним аргументом x и телом t. Каждый шаг вычисления будет заключаться в замене всех вхождений переменной x внутри t на y.

data Expression = Var String
                | Lambda String Expression
                | Apply Expression Expression
                deriving (Eq, Show)


type Context = [(String, Expression)]

--Заносим в context имя переменной и терм, стоящий под этим именем
-- На финальных шагах достает данные из контекста, если найдено
apply :: Context -> Expression -> Expression -> Expression
apply context t1 t2 = case t1 of
                    (Lambda s t)    ->  eval ((s, t2):context) t
                    otherwise       ->  Apply t1 t2


 

--Ищем в контексте переменную и пытаемся подставить
eval :: Context -> Expression -> Expression
eval context exp = case exp of
                (Apply t1 t2)   -> apply context (eval context t1) (eval context t2)
                (Lambda s t)    -> Lambda s (eval context t)
                 l@(Var a)   -> maybe l id (lookup a context)

-- result work
evaluate :: Expression -> Expression
evaluate t = eval [] t

simpleTest = evaluate $ Apply (Lambda "x" (Lambda "x" (Var "x"))) (Lambda "x" (Var "x"))

-- ((\x.x) x)  ->  x
test1 = evaluate $ Apply (Lambda "x" (Var "x")) (Var "x")

-- IT WORKS!
testwrong = evaluate $ Apply (Apply(Lambda "x" (Var "x")) (Lambda "x" (Var "x"))) (Var "x")

--simpliestTestEver = evaluate $ Apply(Lambda "x" (Var "x"))

-- ((\x.(\y.y)) (\z.z))  ->  (\y.y)
test2 = evaluate $ Apply (Lambda "x" (Lambda "y" (Var "y"))) (Lambda "z" (Var "z"))

-- (((\x.(\k.k)) (\m.m)) (\z.z))  ->  (\z.z)
test3 = evaluate $ Apply (Apply (Lambda "x" (Lambda "k" (Var "k"))) (Lambda "m" (Var "m"))) (Lambda "z" (Var "z"))
