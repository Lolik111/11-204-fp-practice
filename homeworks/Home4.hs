
-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex [] = (-1, 0)
myMaxIndex (x:xs) = func (0, x) 1 xs 
	where
		func m k [] = m
	 	func m k (x:xs) | x > (snd m) =  func (k, x) (k + 1) xs
	 						  | otherwise  = func m (k + 1) xs

-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Integer] -> Int
maxCount (x:xs) = func (1, x) xs 
	where
		func m [] = fst m 
	 	func m (x:xs) | x > (snd m) =  func (1, x) xs
	 						| x == (snd m) =  func (fst m + 1, x) xs
	 						| otherwise  = func m xs


myMinIndex :: [Integer] -> (Int, Integer)
myMinIndex [] = (-1, 0)
myMinIndex (x:xs) = minIndex (0, x) 1 xs 
	where
		minIndex m k [] = m
	 	minIndex m k (x:xs) | x < (snd m) =  minIndex (k, x) (k + 1) xs
	 						  | otherwise  = minIndex m (k + 1) xs

-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0
countBetween :: [Integer] -> Int
countBetween [] = 0
countBetween [x] = 0
countBetween arr = (fst $ myMaxIndex arr ) - (fst $ myMinIndex arr)	