module Main where
main :: IO ()


deleteInt :: Int -> [Int] -> [Int]
deleteInt a [] = []
deleteInt a [x] = if a == x then []
                         else [x]
deleteInt a (x:y:xs) = if a == x then deleteInt a $ y:xs
                              else x:(deleteInt a $ y:xs)

findIndicesInt :: Int -> [Int] -> [Int]
--findIndicesInt a [] = []
findIndicesInt a (x:xs) = let
               fun :: Int -> Int -> [Int] -> [Int] -> [Int]
               fun a b [] (y:ys) = y : ys
               fun a b [] [] = []
               fun a b (x:xs) [] = if a == x then fun a (b + 1) (xs) ([b])
                                             else fun a (b + 1) (xs)  ([])
               fun a b (x:xs) (y:ys) = if a == x then fun a (b + 1) (xs) (y:ys ++ [b])
                                               else fun a (b + 1) (xs)  (y:ys)
               in fun a 0 (x:xs) []


main = print (findIndicesInt 1 $ [1,2,3,1,4,1,1,5])